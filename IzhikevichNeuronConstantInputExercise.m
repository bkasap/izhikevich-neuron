
%% Prolog

close all;
clear;

%% Neuron parameters
a=0.01; %(Inverse) recovery time scale
b=0.2; % Coupling strength voltage -> recovery variable
c=-65; % Voltage reset (mV)
d=2; % Recovery variable reset
I0=10; % External current (mA)
vtheta=30; % Potential at which neuron is resetted

%% Simulation parameters
T=280; % Total time (in ms)
dt=0.001; % stepsize for integration

%% Initial values for v and u
v=0;
u=0;

%% Time evolution
t_rec=0:dt:T; % time points list
v_rec=zeros(1,length(t_rec)); % initialize voltage list
u_rec=zeros(1,length(t_rec)); % initialize recovery variable list

t_step=0;
for t=t_rec
    t_step=t_step+1;
    [[?]]%Change in V according to Izhikevich differential equation and Euler integration scheme
    [[?]]%Change in u according to Izhikevich differential equation and Euler integration scheme
    [[?]]%Update v
    [[?]]%Update u
    if (v>vtheta)%Spike generation
       [[?]]%Impact of spike on v
       [[?]]%Impact of spike on u
    end;
    v_rec(t_step)=v;%Save membrane potential to later plot it
    u_rec(t_step)=u;
end;

%% Plotting

figure()
plot(t_rec,v_rec,'b','LineWidth',2);
ylabel('Membrane Potential (mV)');
xlabel('Time (ms)');

figure()
plot(t_rec,u_rec,'r','LineWidth',2);
ylabel('Recovery variable (mA)');
xlabel('Time (ms)');




