
%% Prolog

close all;
clear;



%% Neuron parameters
a=0.01; %(Inverse) recovery time scale
b=0.2; % Coupling strength voltage -> recovery variable
c=-65; % Voltage reset (mV)
d=2; % Recovery variable reset
I0=10; % External current (mA)
vtheta=30; % Potential at which neuron is resetted

%% Simulation parameters
T=280; % Total time (in ms)
dt=0.001; % stepsize for integration

%% Initial values for v and u
v=0;
u=0;

%% Time evolution
t_rec=0:dt:T; % time points list
v_rec=zeros(1,length(t_rec)); % initialize voltage list
u_rec=zeros(1,length(t_rec)); % initialize recovery variable list

t_step=0;
for t=t_rec
    t_step=t_step+1;
    dv=(0.04*v^2+5*v+140-u+I0)*dt;%Change in V according to Izhikevich differential equation and Euler integration scheme
    du=a*(b*v-u)*dt;%Change in u according to Izhikevich differential equation and Euler integration scheme
    v=v+dv;%Update v
    u=u+du;%Update u
    if (v>vtheta)%Spike generation
       v=c;%Impact of spike on v
       u=u+d;%Impact of spike on u
    end;
    v_rec(t_step)=v;%Save membrane potential to later plot it
    u_rec(t_step)=u;
end;

%% Plotting

figure()
plot(t_rec,v_rec,'b','LineWidth',2);

figure()
plot(t_rec,u_rec,'r','LineWidth',2);





