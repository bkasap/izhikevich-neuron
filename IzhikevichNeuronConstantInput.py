import numpy as np
import pylab as pl

## Neuron parameters
a=0.01		# (Inverse) recovery time scale
b=0.2 		# Coupling strength voltage -> recovery variable
c=-65 		# Voltage reset (mV)
d=2		# Recovery variable reset
I0=10		# External current (mA)
vtheta=30	# Potential at which neuron is resetted

## Simulation parameters
T=280		# Total time (in ms)
dt=0.001	# stepsize for integration

## Initial values for v and u
v=0
u=0

## Time evolution
t_rec=np.arange(0, T, dt)	# time points list
v_rec=np.zeros(len(t_rec))	# initialize voltage list
u_rec=np.zeros(len(t_rec))	# initialize recovery variable list

t_step=0
for t in t_rec:
    # Change in V according to Izhikevich differential equation and Euler integration scheme
    dv=(0.04*v**2+5*v+140-u+I0)*dt
    # Change in u according to Izhikevich differential equation and Euler integration scheme
    du=a*(b*v-u)*dt
    # Update v
    v=v+dv
    # Update u
    u=u+du
    if (v>vtheta):	# Spike generation
       v=c		# Impact of spike on v
       u=u+d		# Impact of spike on u
    v_rec[t_step]=v	# Save membrane potential to later plot it
    u_rec[t_step]=u
    t_step=t_step+1;

## Plotting

pl.figure()
pl.plot(t_rec,v_rec,'b',lw=2)
pl.ylabel('Membrane Potential (mV)')
pl.xlabel('Time (ms)')

pl.figure()
pl.plot(t_rec,u_rec,'r',lw=2)
pl.ylabel('Recovery variable (mA)')
pl.xlabel('Time (ms)')
pl.show()




