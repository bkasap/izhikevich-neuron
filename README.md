# README #

Matlab and python code for Izhikevich neuron model responses to constant current.

### Includes exercises ###

* Izhikevich neuron model
* State variable updates with Euler integration scheme
* Plotting membrane potential and recovery variable

### How do I get set up? ###

* Scripts are independent. Exercise files are provided for Neurophysics students.
* Clone or download the repository and run the files.

### Contribution guidelines ###

* Feedbacks, comments and push requests are welcomed.

### To-do ###

* Spike trains can be saved and plotted as well.

### Contact ###

* Contact me from repo owner.